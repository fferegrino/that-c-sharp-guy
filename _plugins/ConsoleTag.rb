module Jekyll
  class ConsoleTag < Liquid::Block
    @title = nil

    def initialize(tag_name, markup, tokens)
      @title = markup
      super
    end

    def render(context)
      output = super(context)
      "<figure class=\"console\"><pre><code>#{output.strip}</code></pre></figure>"
    end
  end
end

Liquid::Template.register_tag('console', Jekyll::ConsoleTag)