---
layout: page
title: Acerca de
permalink: /acerca-de/
lang: es
---
## Antonio Feregrino Bolaños  
Programador, con ganas de enseñar y muchas más de aprender. Apasionado de lo que hago, comencé en 2008 con C# y no he parado, llevo 6 años de universidad a la par de trabajar escribiendo código en Heurística Soluciones. También fui Microsoft Student Partner y actualmente soy Xamarin Student Partner.

**Esto es importante: Me gusta C# pero siempre he creído que lo importante es tener buenas ideas y saber expresarlas, el lenguaje es lo de menos.**


## Sobre el blog
Este es un blog dedicado a las ciencias computacionales y a la programación (más a esto último) de aplicaciones. El blog está hecho con [Jekyll en una computadora con Windows 8.1](http://jekyll-windows.juthilo.com) y utilizando [Code](https://code.visualstudio.com) como editor principal. Por alguna razón todo el código de este blog está en [GitHub](http://github.com/fferegrino/that-c-sharp-guy) por si lo quieres ver.  

#### Citando  
Si quieres citar alguno de mis posts, puedes hacerlo, lo que pido es que agregues una referencia hacia este sitio (preferentemente a la parte que estás citando).
  
#### Links de afiliado  
En ciertas ocasiones, es posible que un post contenga links de afiliado, estos son enlaces especiales a productos que cuando accedes a la tienda en línea a través de ellos y compras algo, yo recibo una comisión. Sin embargo todos los links de este tipo son a productos que he probado y confío o que estoy pensando en probar; habiendo dicho esto, ten por seguro que los hago con el único motivo de darte aún más valor al recomendarte un producto que te puede ser de utilidad y a la vez conseguir financiación para mantener el blog.