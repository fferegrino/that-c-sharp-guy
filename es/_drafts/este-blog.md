---
layout: post
title: "Sobre este blog"
date:  2016-03-20 18:31:38
author: Antonio Feregrino Bolaños
categories: blog es
excerpt: Este es un post en el que explico cómo está hecho este blog, por qué está hecho así y qué es lo que uso para hacerlo. 
lang: es
alias: /hola-mundo/index.html
tags:
- Meta
featured_tag: Meta
---
Este no es el primer blog en el que escribo (pero si en el que he mantenido una constancia), anteriormente había tenido experiencia en plataformas como Blogger con <a href="http://risaacida.blogspot.mx/" target="_blank" rel="nofollow">RisaÁcida</a> y con la primera versión de mi blog, <a href="http://fferegrino.blogspot.mx/" rel="nofollow" target="blank">El Blog de Feregrino</a>. También he usado cosas como Wordpress, para la segunda versión de mi blog. Y ni hablar de Ghost, en el blog de XSA... la mayoría de estos están en abandono.


## Jekyll

### Plugins  
 
#### AliasGenerator  
Lo que hace este plugin es generar una URL dentro del servidor que apunte a un archivo que contiene una redirección hacia otro archivo dentro de ese mismo servidor. Yo la empecé a usar porque al inicio de este blog todos los post estaban directamente sobre la raíz y no como ahora que se encuentran en el subdirectorio `posts`. Cuando cambié los posts viejos hacia dentro del subdirectorio los antigüos enlaces no funcionaban así que supuse que era necesario una redirección.

#### ConsoleTag  
Este plugin es algo muy, muy sencillo ya que lo único que hace es envolver en el siguiente HTML el código que se encuentra entre las etiquetas `console` y `endconsole`:
{% highlight html %}
<figure class="console">
    <pre>
        <code>
            <!-- Contenido -->
        </code>
    </pre>
</figure>
{% endhighlight %}

De tal manera que con esta etiqueta:

<pre>
{&#37; console &#37;}
Hola mundo!
{&#37; endconsole &#37;}
</pre>

Se genera este resultado:

{% console %}
Hola mundo!
{% endconsole %}

Por cierto, este plugin si es de mi autoría.

#### PostImageTag
Cree este otro plugin para tener una manera simple de añadir imágenes a cada post, ya que únicamente se requiere de indicar el nombre del archivo ya que internamente toma la url del post para generar la ruta hacia la imagen. Por ejemplo, si escribimos este código:

<pre>
{&#37; post_image dizzy.png &#37;}
</pre>


Bastará con que dentro de la ruta `/postimages/este-blog/` exista un archivo llamado `dizzy.png`, entonces se producirá el siguiente resultado:

{% post_image dizzy.png %}
   

#### TagPageGenerator 
Como su nombre lo indica, este plugin se usa para generar páginas de tags, <a href="http://thatcsharpguy.com/tag/AprendeCSharp/">como esta</a>, para eso necesita además de dos plantillas auxiliares, una para listar todas las etiquetas dentro del blog y otra para mostrar qué posts tienen determinada etiqueta.