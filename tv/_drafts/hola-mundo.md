---
title:  "¡Hola mundo!"
date:   2015-12-16 18:00:00
youtube: "oS9chdlUnFI"
categories: c-sharp
excerpt: Video de presentación de 
featured_image: featured.png
lang: es
images_folder: /tv/hola-mundo/
featured_image: featured.png
---
Este es el video de presentación de este nuevo canal de YouTube en donde, a pesar del nombre, no solamente hablaré de C#, si no de varias muchas otras cosas relacionadas con la programación.

En este canal podrás encontrar:  

 - Explicaciones de patrones de diseño, conceptos de programación y algoritmos
 - Reseñas de aplicaciones para desarrollar 
 - Reseñas de librerías, estas si serán específicas para C#
 
 
Y temas de este tipo, te invito a que te suscribas en el botón que está un poco más abajo. Y pues nos vemos en el siguiente video.