---
layout: post-video
title: "Ideone"
date: 2016-03-01 18:00:00
youtube: "oS9chdlUnFI"
is_tv: true
images_folder: /tv/ideone/
categories: c-sharp
excerpt: This guide will take you step by step to create your first Xamarin.Forms app.
featured_image: featured.png
---
Ideone es <s>un compilador en línea</s> una colección de compiladores en línea, que permite escribir, compilar y ejecutar código fuente escrito en más de 60 lenguajes de programación. Basta con entrar a la página web, escribir o copiar tu código dar click en *Run* y listo, el código se ejecuta.

Hay una cantidad bastante grande de lenguajes con los que funciona, entre ellos C, C++, Java y, claro que sí, C#. 

{% post_image langs.png "Lenguajes soportados" %}

### ¿Es un IDE?  
El nombre puede resultar un poco confuso, y es que a pesar de lo que se podría pensar al ver solo el nombre, la parte del IDE, no lo podemos comparar con otras herramientas como NetBeans o Visual Studio puesto que como se ve, únicamente tenemos acceso a un editor bastante sencillo de código y un botón de "Ejecutar".  

{% post_image run.png "Lenguajes soportados" %}

### Entradas y salidas  
Para simular la entrada y salida estandar, digamos, el teclado y la pantalla, tenemos estas dos cajas de texto. La gran diferencia es que no es interactivo, tenemos que preeestablecer la entrada desde un inicio, en otras palabras, no existe interacción porque no puedes escribir al tiempo que se ejecuta tu prorgrama.  

### ¿Qué se puede hacer con él?  
 
 - Lo que se puede hacer con una aplicación de consola recién creada: salidas y entradas de texto.
 - Crear breves fragmentos de código y compartirlos con otras personas, ellos pueden ver el código, copiarlo y compilarlo.
 - Llevar un registro de los códigos que hemos creado, así como etiquetarlos para crear colecciones y tener un poco más de control sobre ellos.  

### ¿Qué no se puede hacer con él?
No podemos agregar una referencia a algo no incluído dentro de la especificación del compilador y esperar que tu código compile, por ejemplo, si usas C# como lenguaje, no puedes agregar una referencia a un paquete de NuGet.

También se tiene que tomar en cuenta de que la ejecución se realiza en una “caja de arena” o *sandbox* para impedir que se acceda al sistema de archivos o se modifiquen cosas como el registro o el sistema operativo en el que los programas son ejecutadas.

## Ejemplos de uso

 - Probar ideas de código de manera rápida, por ejemplo, en el Club de Algoritmia de la ESCOM yo lo usaba para probar algunos programas y poder compartirlos con mis compañeros de equipo para darnos una idea de qué tan buena era nuestra solución.
 
 - Yo lo uso en el blog para algunos de mis posts, tal vez se hayan dado cuenta de que en algunos dentro de la sección de información, lo único que tienen que hacer es dar click justo encima del título del post y listo.
 
 - Se puede usar para empezar a programar en algún lenguaje sin necesidad de tener las herramientas instaladas en la computadora desde la que estemos trabajando.